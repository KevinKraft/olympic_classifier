import pandas as pd

TRAINING_DATA = 'data/training/unscaled_training_2021_07_31.csv'

#NB: Scaling is neessary
#
# https://datascience.stackexchange.com/questions/60950/is-it-necessary-to-normalise-data-for-xgboost/60954

def main():

    df_input = pd.read_csv(TRAINING_DATA)
    print(df_input)

    print(df_input.dtypes)

if __name__ == '__main__':
    main()