import pandas as pd

GDP_DATA_PATH = 'data/raw/current/gdp_by_country.csv'

POP_DATA_PATH = 'data/raw/current/population_by_country.csv'


def main():

    #---------------------------------------------------------------------------------------------------
    # current GDP dataset
    #---------------------------------------------------------------------------------------------------

    #load the current gdp data and clean
    df_gdp = pd.read_csv(GDP_DATA_PATH)[['Country Name', '2020 [YR2020]', '2019 [YR2019]', '2018 [YR2018]',
                                         '2017 [YR2017]', '2016 [YR2016]']].dropna(subset=['Country Name'])
    df_gdp['Country Name'] = df_gdp['Country Name'].str.lower() \
                                                   .str.replace(' ', '_', regex=False) \
                                                   .str.strip()
    print(df_gdp)
    print(len(df_gdp))
    print(len(df_gdp.drop_duplicates(['Country Name']).reset_index()))

    for col_name in ['2020 [YR2020]', '2019 [YR2019]', '2018 [YR2018]', '2017 [YR2017]', '2016 [YR2016]']:
        df_gdp = df_gdp[df_gdp[col_name] != '..']
        df_gdp[col_name] = df_gdp[col_name].astype(float).round()

    df_gdp = df_gdp.rename(columns={
        '2020 [YR2020]': '2020',
        '2019 [YR2019]': '2019',
        '2018 [YR2018]': '2018',
        '2017 [YR2017]': '2017',
        '2016 [YR2016]': '2016',
    })

    #pivot to get one record per year
    df_gdp = df_gdp.set_index('Country Name')
    df_gdp = df_gdp.stack()
    df_gdp = df_gdp.reset_index()
    df_gdp = df_gdp.rename(columns={0: 'gdp', 'level_1': 'year', 'Country Name': 'country'})
    print(df_gdp)

    #get gdp change in country groups
    df_gdp['gdp_pct'] = df_gdp.sort_values('year').groupby(['country'])['gdp'].pct_change().fillna(0)
    df_gdp = df_gdp.reindex()
    print(df_gdp)

    #get current and last 4 years data as separate columns
    for col_name in ['gdp', 'gdp_pct']:
        for i in range(2, 5):
            new_col_name = f'{col_name}_prev_{i}'
            df_gdp[new_col_name] = df_gdp.sort_values('year') \
                                                   .groupby(['country'])[col_name] \
                                                   .apply(lambda x: x.shift(i - 1))
            df_gdp[new_col_name] = df_gdp[new_col_name].fillna(0)
    df_gdp = df_gdp.rename(columns={'gdp': 'gdp_prev_1', 'gdp_pct': 'gdp_pct_prev_1'})
    df_gdp = df_gdp[df_gdp['year'] == '2020']
    print(df_gdp)

    df_gdp.to_csv('data/tmp/currect_gdp.csv', index=False)

    #---------------------------------------------------------------------------------------------------
    # current population dataset
    #---------------------------------------------------------------------------------------------------

    #load the current gdp data and clean
    df_pop = pd.read_csv(POP_DATA_PATH)[['Country Name', '2020 [YR2020]', '2019 [YR2019]', '2018 [YR2018]',
                                         '2017 [YR2017]', '2016 [YR2016]']].dropna(subset=['Country Name'])
    df_pop['Country Name'] = df_pop['Country Name'].str.lower() \
                                                   .str.replace(' ', '_', regex=False) \
                                                   .str.strip()
    print(df_pop)
    print(len(df_pop))
    print(len(df_pop.drop_duplicates(['Country Name']).reset_index()))

    for col_name in ['2020 [YR2020]', '2019 [YR2019]', '2018 [YR2018]', '2017 [YR2017]', '2016 [YR2016]']:
        df_pop = df_pop[df_pop[col_name] != '..']
        df_pop[col_name] = df_pop[col_name].astype(float).round()

    df_pop = df_pop.rename(columns={
        '2020 [YR2020]': '2020',
        '2019 [YR2019]': '2019',
        '2018 [YR2018]': '2018',
        '2017 [YR2017]': '2017',
        '2016 [YR2016]': '2016',
    })

    #pivot to get one record per year
    df_pop = df_pop.set_index('Country Name')
    df_pop = df_pop.stack()
    df_pop = df_pop.reset_index()
    df_pop = df_pop.rename(columns={0: 'pop', 'level_1': 'year', 'Country Name': 'country'})
    print(df_pop)

    #get gdp change in country groups
    df_pop['pop_pct'] = df_pop.sort_values('year').groupby(['country'])['pop'].pct_change().fillna(0)
    df_pop = df_pop.reindex()
    print(df_pop)

    #get current and last 4 years data as separate columns
    for col_name in ['pop', 'pop_pct']:
        for i in range(2, 5):
            new_col_name = f'{col_name}_prev_{i}'
            df_pop[new_col_name] = df_pop.sort_values('year') \
                                                   .groupby(['country'])[col_name] \
                                                   .apply(lambda x: x.shift(i - 1))
            df_pop[new_col_name] = df_pop[new_col_name].fillna(0)
    df_pop = df_pop.rename(columns={'pop': 'pop_prev_1', 'pop_pct': 'pop_pct_prev_1'})
    df_pop = df_pop[df_pop['year'] == '2020']
    print(df_pop)

    df_pop.to_csv('data/tmp/currect_pop.csv', index=False)

    #---------------------------------------------------------------------------------------------------
    # join results and save
    #---------------------------------------------------------------------------------------------------

    df_gdp_pop = df_gdp.merge(df_pop, on=['country', 'year'])
    df_gdp_pop = df_gdp_pop[['country', 'year', 'gdp_prev_1', 'gdp_prev_2', 'gdp_prev_3', 'gdp_prev_4',
                             'gdp_pct_prev_1', 'gdp_pct_prev_2', 'gdp_pct_prev_3', 'gdp_pct_prev_4', 'pop_prev_1',
                             'pop_prev_2', 'pop_prev_3', 'pop_prev_4', 'pop_pct_prev_1', 'pop_pct_prev_2',
                             'pop_pct_prev_3', 'pop_pct_prev_4']]
    print(df_gdp_pop)
    df_gdp_pop.to_csv('data/tokyo_2021_input.csv', index=False)


if __name__ == '__main__':
    main()