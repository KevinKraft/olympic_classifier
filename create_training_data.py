import pandas as pd
from datetime import date

MEDAL_DATA_PATH = 'data/raw/medals/kaggle/athlete_events.csv'
MEDAL_COUNTRY_NAMES_PATH = 'data/raw/medals/kaggle/noc_regions.csv'
GDP_DATA_PATH = 'data/raw/gdp/gdp_by_country.csv'
POP_DATA_PATH = 'data/raw/population/population_by_country.csv'

COUNTRY_COL = 'country'
YEAR_COL = 'year'
NUM_MEDALS_COL = 'n_medals'


def main():

    #---------------------------------------------------------------------------------------------------
    # Olympic medal dataset
    #---------------------------------------------------------------------------------------------------

    # Get the total number of medals per country code per year

    df_medals = pd.read_csv(MEDAL_DATA_PATH)[['NOC', 'Year', 'Season', 'Event', 'Medal']]

    #get list of season field values
    medal_seasons = set(df_medals['Season'].unique())
    print(medal_seasons)
    print(len(medal_seasons))

    #get list of medal country codes
    medal_country_codes = set(df_medals['NOC'].unique())
    print(medal_country_codes)
    print(len(medal_country_codes))

    #get list of years
    medal_years = set(df_medals['Year'].unique())
    print(medal_years)
    print(len(medal_years))

    #get list of medal field values
    medal_fields = set(df_medals['Medal'].unique())
    print(medal_fields)
    print(len(medal_fields))

    #get summer only
    df_medals = df_medals[df_medals['Season'] == 'Summer'].drop(['Season'], axis=1)

    #get country names
    df_medal_country_names = pd.read_csv(MEDAL_COUNTRY_NAMES_PATH)
    df_medal_country_names['region'] = df_medal_country_names['region'].str.lower() \
                                                                       .str.replace(' ', '_', regex=False) \
                                                                       .str.strip()
    print(df_medal_country_names)

    #join to country names by code
    df_medals = df_medals.merge(df_medal_country_names, left_on='NOC', right_on='NOC',
                                how='left')[['region', 'Year', 'Event', 'Medal']]
    print(df_medals)

    #drop event duplicates, because teams get more than one medal
    df_medals = df_medals.drop_duplicates(['region', 'Year', 'Event']).drop(['Event'], axis=1)
    print(df_medals)

    #get medal count per country code, per year. Count automatically ignores nulls.
    df_medals = df_medals.groupby(['region', 'Year'], as_index=False).count()
    print(df_medals)
    print(df_medals.index)
    print(df_medals[df_medals['region'] == 'zimbabwe'])

    #country name mappings
    medal_to_gdp_name_mappings = {
        'uk': 'united_kingdom',
        'usa': 'united_states',
        'trinidad': 'trinidad_and_tobago',
        'antigua': 'antigua_and_barbuda',
        'boliva': 'bolivia',
        'saint_kitts': 'st._kitts_and_nevis',
        'saint_lucia': 'st._lucia',
        'saint_vincent': 'st._vincent_and_the_grenadines',

    }
    df_medals['region'] = df_medals['region'].map(lambda x: medal_to_gdp_name_mappings.get(x, x))
    print(df_medals)

    #rename medal columns
    df_medals = df_medals.rename(columns={
        'region': COUNTRY_COL,
        'Year': YEAR_COL,
        'Medal': NUM_MEDALS_COL,
    }).reindex()
    print(df_medals)

    #check outputs
    print(df_medals[df_medals[COUNTRY_COL].isna()])
    print(df_medals[df_medals[YEAR_COL].isna()])
    print(df_medals[df_medals[NUM_MEDALS_COL].isna()])

    #get list of country names
    medal_countries = set(df_medals[COUNTRY_COL].unique())
    print(medal_countries)
    print(len(medal_countries))

    df_medals.to_csv('data/tmp/medals.csv', index=False)

    #---------------------------------------------------------------------------------------------------
    # GDP dataset
    #---------------------------------------------------------------------------------------------------

    # Get the country GDP value and percentage change by country and year. For each olympic year, get the GDP
    # and GDP change for the olympic year, and the 4 years prior to the olympics (5 years total). This is because
    # the summer olympics only happens a few months into the FY, so the current years GDP might not be the best
    # predictor.

    df_gdp = pd.read_csv(GDP_DATA_PATH)[['name', 'time', 'GDP total']].rename(columns={'GDP total': 'gdp'})
    df_gdp['name'] = df_gdp['name'].str.lower() \
                                   .str.replace(' ', '_', regex=False) \
                                   .str.strip()
    print(df_gdp)

    #country name mappings
    gdp_to_medal_name_mappings = {
        "cote_d'ivoire": 'ivory_coast',
        'kyrgyz_republic': 'kyrgyzstan',
        'macedonia,_fyr': 'macedonia',
        'slovak_republic': 'slovakia',
        'hong_kong,_china': 'hong_kong',
        'congo,_dem._rep.': 'democratic_republic_of_the_congo',
        'congo,_rep.': 'republic_of_congo',
        'lao': 'laos',
        'micronesia,_fed._sts.': 'micronesia',
    }
    df_gdp['name'] = df_gdp['name'].map(lambda x: gdp_to_medal_name_mappings.get(x, x))
    print(df_gdp)

    #get set of countries and compare
    gdp_countries = set(df_gdp['name'].unique())
    print('All GDP countries:', gdp_countries)
    print('N GDP countries:', len(gdp_countries))
    print('N medal countries:', len(medal_countries))
    print('GDP-medal country count:', len(gdp_countries) - len(medal_countries))
    print('GDP diff medal countries:', sorted(gdp_countries.difference(medal_countries)))
    print('GDP diff medal country count:', len(gdp_countries.difference(medal_countries)))
    print('medal diff GDP countries:', sorted(medal_countries.difference(gdp_countries)))
    print('medal diff GDP country count:', len(medal_countries.difference(gdp_countries)))

    #get gdp change in country groups
    df_gdp['gdp_pct'] = df_gdp.sort_values('time').groupby(['name'])['gdp'].pct_change().fillna(0)
    df_gdp = df_gdp.reindex()
    print(df_gdp)

    #get current and last 4 years data as separate columns
    for col_name in ['gdp', 'gdp_pct']:
        for i in range(1, 5):
            new_col_name = f'{col_name}_prev_{i}'
            df_gdp[new_col_name] = df_gdp.sort_values('time') \
                                                   .groupby(['name'])[col_name] \
                                                   .apply(lambda x: x.shift(i))
            df_gdp[new_col_name] = df_gdp[new_col_name].fillna(0)
    print(df_gdp)

    #rename columns
    df_gdp = df_gdp.rename(columns={
        'name': COUNTRY_COL,
        'time': YEAR_COL,
    }).reindex()
    print(df_gdp)

    #join to the medal data
    df_medal_gdp = df_medals.merge(df_gdp, on=[COUNTRY_COL, YEAR_COL])

    #get list of all countries
    medal_gdp_countries = set(df_medal_gdp[COUNTRY_COL].unique())
    print(medal_gdp_countries)
    print(len(medal_gdp_countries))
    print('comb to gdp diff: ', medal_gdp_countries.difference(gdp_countries))
    print('gdp to comb diff: ', gdp_countries.difference(medal_gdp_countries))
    print('comb to medals diff: ', medal_gdp_countries.difference(medal_countries))
    print('medals to comb diff: ', medal_countries.difference(medal_gdp_countries))

    df_gdp.to_csv('data/tmp/gdp.csv', index=False)

    #---------------------------------------------------------------------------------------------------
    # Population by country
    #---------------------------------------------------------------------------------------------------

    # Get the country population by year, and the last four years of population and population change data.
    df_pop = pd.read_csv(POP_DATA_PATH)[['name', 'time', 'Population']].rename(columns={'Population': 'pop'})
    df_pop['name'] = df_pop['name'].str.lower() \
                                   .str.replace(' ', '_', regex=False) \
                                   .str.strip()
    print(df_pop)

    #get pop change in country groups
    df_pop['pop_pct'] = df_pop.sort_values('time').groupby(['name'])['pop'].pct_change().fillna(0)
    df_pop = df_pop.reindex()
    print(df_pop)

    #get current and last 4 years data as separate columns
    for col_name in ['pop', 'pop_pct']:
        for i in range(1, 5):
            new_col_name = f'{col_name}_prev_{i}'
            df_pop[new_col_name] = df_pop.sort_values('time') \
                                         .groupby(['name'])[col_name] \
                                         .apply(lambda x: x.shift(i))
            df_pop[new_col_name] = df_pop[new_col_name].fillna(0)
    df_pop = df_pop.reindex()
    print(df_pop)

    #country name mappings
    pop_to_medal_name_mappings = {
        "cote_d'ivoire": 'ivory_coast',
        'kyrgyz_republic': 'kyrgyzstan',
        'macedonia,_fyr': 'macedonia',
        'slovak_republic': 'slovakia',
        'hong_kong,_china': 'hong_kong',
        'congo,_dem._rep.': 'democratic_republic_of_the_congo',
        'congo,_rep.': 'republic_of_congo',
        'lao': 'laos',
        'micronesia,_fed._sts.': 'micronesia',
    }
    df_pop['name'] = df_pop['name'].map(lambda x: pop_to_medal_name_mappings.get(x, x))
    print(df_pop)

    #get set of countries and compare
    pop_countries = set(df_pop['name'].unique())
    print('All pop countries:', pop_countries)
    print('pop diff comb countries:', sorted(pop_countries.difference(medal_gdp_countries)))
    print('comb diff pop countries:', sorted(medal_gdp_countries.difference(pop_countries)))

    #rename columns
    df_pop = df_pop.rename(columns={
        'name': COUNTRY_COL,
        'time': YEAR_COL,
    }).reindex()
    print(df_pop)

    #join to the rest of the data
    df_medal_gdp_pop = df_medal_gdp.merge(df_pop, on=[COUNTRY_COL, YEAR_COL])

    df_pop.to_csv('data/tmp/pop.csv', index=False)

    #---------------------------------------------------------------------------------------------------
    # Scale n_medals
    #---------------------------------------------------------------------------------------------------

    #normalise n_medals by how many were available.
    df_medal_gdp_pop['_num_medals_in_year'] = df_medal_gdp_pop.groupby([YEAR_COL])[NUM_MEDALS_COL].transform('sum')
    df_medal_gdp_pop[NUM_MEDALS_COL] = df_medal_gdp_pop[NUM_MEDALS_COL] / df_medal_gdp_pop['_num_medals_in_year']
    df_medal_gdp_pop = df_medal_gdp_pop.drop(['_num_medals_in_year'], axis=1)
    df_medal_gdp_pop = df_medal_gdp_pop.drop(['gdp', 'pop', 'gdp_pct', 'pop_pct'], axis=1)
    print(df_medal_gdp_pop)

    df_medal_gdp_pop.to_csv(f"data/training/unscaled_training_{str(date.today()).replace('-', '_')}.csv", index=False)


if __name__ == '__main__':
    main()

