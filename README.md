Predict the number of medals won by each country at the Olympics.

It uses the previous four years of country GDP, population, GDP percentage change per year, and population change per year.