import os
import pandas as pd
import xgboost as xgb
from sklearn.model_selection import train_test_split
from datetime import date
import matplotlib.pyplot as plt

TRAINING_DATA = 'data/training/unscaled_training_2021_07_31.csv'
TMP_DIR = os.path.join('data', 'tmp')
MODEL_PATH = os.path.join('data', 'models')
TOKYO_DATA = 'data/tokyo_2021_input.csv'

COUNTRY_COL = 'country'
YEAR_COL = 'year'
NUM_MEDALS_COL = 'n_medals'


def main():

    df_input = pd.read_csv(TRAINING_DATA).drop([COUNTRY_COL, YEAR_COL], axis=1)

    print(df_input)

    #split training and testing data
    df_train, df_test = train_test_split(df_input, test_size=0.3)
    test_expected = df_test[NUM_MEDALS_COL]

    #load data in xgb objects
    dm_train = xgb.DMatrix(df_train.drop([NUM_MEDALS_COL], axis=1), label=df_train[NUM_MEDALS_COL])
    dm_test = xgb.DMatrix(df_test.drop([NUM_MEDALS_COL], axis=1), label=df_test[NUM_MEDALS_COL])
    print(dm_train.feature_names)

    #set params
    model_params = {
        'max_depth': 2,
        'eta': 1,
        'objective': 'binary:logistic',
        'nthread': 4,
        'eval_metric': ['auc', 'ams@0'],
    }
    model_evallist = [(dm_test, 'eval'), (dm_train, 'train')]
    num_round = 15

    xgb_model = xgb.train(model_params, dm_train, num_round, model_evallist)

    model_basepath = os.path.join(MODEL_PATH, f"xgb_{str(date.today()).replace('-', '_')}")
    xgb_model.save_model(f'{model_basepath}.model')
    #xgb_model.dump_model(f'{model_basepath}.raw.txt', f'{model_basepath}_featuremap.txt')

    #plot model performace
    #xgb.plot_importance(xgb_model)
    #xgb.plot_tree(xgb_model, num_trees=10)
    #plt.show()

    #get predictions on the test data
    test_pred = xgb_model.predict(dm_test)
    df_test = pd.concat([df_test.reset_index(), pd.Series(test_pred)], axis=1)
    print(df_test)
    df_test.to_csv('data/tmp/df_test.csv', index=False)

    #get predictions on this years olympics
    df_tokyo = pd.read_csv(TOKYO_DATA)
    dm_tokyo = xgb.DMatrix(df_tokyo.drop(['country', 'year'], axis=1))

    tokyo_pred = xgb_model.predict(dm_tokyo)
    df_tokyo = pd.concat([df_tokyo.reset_index(), pd.Series(tokyo_pred)], axis=1).rename(columns={0: 'pred'})
    df_tokyo = df_tokyo[['country', 'pred']]
    #multiply by the number of medals
    df_tokyo['n_medals_pred'] = df_tokyo['pred'] * 339 * 3
    df_tokyo = df_tokyo.drop(['pred'], axis=1)
    print(df_tokyo)
    df_tokyo.to_csv('data/tokyo_2021_predictions.csv', index=False)

if __name__ == '__main__':
    main()